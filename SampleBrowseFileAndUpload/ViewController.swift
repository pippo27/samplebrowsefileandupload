//
//  ViewController.swift
//  SampleBrowseFileAndUpload
//
//  Created by Arthit Thongpan on 8/20/16.
//  Copyright © 2016 Arthit Thongpan. All rights reserved.
//


import UIKit
import Photos
import MobileCoreServices

class ViewController: UIViewController {
    
    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    let imageManager = PHCachingImageManager()
    var asset: PHAsset? {
        didSet {
            self.configureImageForVideo()
        }
    }
    
    var image: UIImage? {
        didSet {
            self.configureImage()
        }
    }
    
    func configureImage() {
        imageView.image = image
    }
    
    func configureImageForVideo() {
        // Update the user interface for the detail item.
        if let asset = self.asset {
            if let label = self.detailDescriptionLabel {
                label.text = asset.localIdentifier
            }
            
            if let imageView = self.imageView {
                let options = PHImageRequestOptions()
                options.resizeMode = .exact;
                let retinaMultiplier = UIScreen.main.scale
                let retinaSquare = CGSize(width:imageView.bounds.size.width * retinaMultiplier, height:imageView.bounds.size.height * retinaMultiplier)
                
                imageManager.requestImage(for: asset, targetSize: retinaSquare, contentMode: .aspectFill, options: options) { (image, _) in
                    DispatchQueue.main.async {
                        imageView.image = UIImage(cgImage: image!.cgImage!, scale: retinaMultiplier, orientation: image!.imageOrientation)
                        self.view.setNeedsLayout()
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    
    func convertCGImageToCIImage(inputImage: CGImage) -> CIImage! {
        let ciImage = CIImage(cgImage: inputImage)
        return ciImage
    }
    
    func convertCIImageToCGImage(inputImage: CIImage) -> CGImage! {
        let context = CIContext(options: nil)
        return context.createCGImage(inputImage, from: inputImage.extent)
    }
    
    func getAssetUrl(asset : PHAsset, completionHandler : ((_ responseURL : URL?) -> Void)){
        
        if asset.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            
            asset.requestContentEditingInput(with: options, completionHandler: { (contentEditingInput:PHContentEditingInput?, info: [AnyHashable : Any]) in
                completionHandler(contentEditingInput!.fullSizeImageURL)
            })
            
            /*
            let option = PHImageRequestOptions()
            option.isSynchronous = true
            option.version = .current
            option.resizeMode = .none
            
            PHImageManager.default().requestImageData(for: asset, options: option, resultHandler: { (data, str, imageOrientation, info:[AnyHashable : Any]?) in
                
             })
 */
            
        } else if asset.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            
            PHImageManager.default().requestAVAsset(forVideo: asset, options: options, resultHandler: { (asset:AVAsset?, audioMix:AVAudioMix?, info:[AnyHashable : Any]?) in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl = urlAsset.url
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }

            })
        }
        
    }
   
    func getFileName(asset:PHAsset) -> String? {
        var fileName:String?
        if #available(iOS 9.0, *) {
            let resources = PHAssetResource.assetResources(for: asset)
            if let resource = resources.first {
                fileName = resource.originalFilename
            }
        }
        if fileName == nil {
            // this is an undocumented workaround that works as of iOS 9.1
            fileName = asset.value(forKey: "filename") as? String
        }
        
        return fileName
    }
    
    func getImageData(image:UIImage)-> Data? {
        return UIImageJPEGRepresentation(image, 1)
    }
    
    func sendDataToServer(data:Data, fileName:String, mediaType:PHAssetMediaType) {
        UploadController.shared.upload(data: data, filename: fileName, mediaType: (asset?.mediaType)!) { (data, response, error) in
            
            DispatchQueue.main.sync {
                if error != nil {
                    self.showAlert(title: "Error", message: error!.localizedDescription)
                    return
                }
                
                if data == nil {
                    self.showAlert(title: "Error", message: "Data not found.")
                    return
                }
                
                self.showAlert(title: "Upload", message: "Success.")
            }
        }
    }
    
    func uploadImage(assetImage:PHAsset, image:UIImage, fileName:String) {
        guard let data = getImageData(image: image) else {
            showAlert(title: "Error", message: "Can not upload.")
            return
        }
        
        sendDataToServer(data: data, fileName: fileName, mediaType: assetImage.mediaType)
    }

    func uploadVideo(assetVideo:PHAsset, fileName:String) {
        getAssetUrl(asset: assetVideo) { (url) in
            guard let data =  FileManager.default.contents(atPath: (url?.path)!) else {
                self.showAlert(title: "Error", message: "Can not upload.")
                return
            }
            
            self.sendDataToServer(data: data, fileName: fileName, mediaType: assetVideo.mediaType)
        }
    }
    
    @IBAction func browseFile(sender: AnyObject) {
        guard UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) == true else { return }
      
        let mediaUI = UIImagePickerController()
        mediaUI.sourceType = .savedPhotosAlbum
        let mediaTypes: Array<String> = [(kUTTypeMovie as String), (kUTTypeImage as String)]
        mediaUI.mediaTypes = mediaTypes
        mediaUI.videoQuality = .typeHigh
        mediaUI.allowsEditing = true
        mediaUI.delegate = self
        present(mediaUI, animated: true, completion: nil)
    }
    
    @IBAction func uploadFile(_ sender: AnyObject) {
        
        if asset == nil {
            showAlert(title: "Error", message: "File not found.")
            return
        }
        
        guard let fileName = getFileName(asset: asset!) else {
            showAlert(title: "Error", message: "File name not found.")
            return
        }
        
        if asset!.mediaType == .video {
            uploadVideo(assetVideo: asset!, fileName: fileName)
            
        } else if asset!.mediaType == .image {
            uploadImage(assetImage: asset!, image: image!, fileName: fileName)
        }
    }
    
    func showAlert(title:String, message:String) {
        UIAlertController.showAlertControllerOnHostController(hostViewController: self,
                                                              title: title,
                                                              message: message,
                                                              buttonTitle: "OK")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

// MARK: - UIImagePickerControllerDelegate

extension ViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        dismiss(animated: true) {
            // Assets Library framework
            let url = info[UIImagePickerControllerReferenceURL] as! NSURL
            let fetchResult = PHAsset.fetchAssets(withALAssetURLs: [url as URL], options: nil)
            
            if let asset = fetchResult.firstObject{
                self.asset = asset
            }
            
            if mediaType == kUTTypeImage {
                
                self.image = info[UIImagePickerControllerOriginalImage] as? UIImage
            }
        }
    }
}

// MARK: - UINavigationControllerDelegate

extension ViewController: UINavigationControllerDelegate {
    
}


