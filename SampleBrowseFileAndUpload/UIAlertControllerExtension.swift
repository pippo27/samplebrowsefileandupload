//
//  UIAlertControllerExtension.swift
//  BasicUploadFile
//
//  Created by Arthit Thongpan on 4/30/16.
//  Copyright © 2016 Arthit Thongpan. All rights reserved.
//

import UIKit

extension UIAlertController{
    class func showAlertControllerOnHostController(
        hostViewController: UIViewController,
        title: String,
        message: String,
        buttonTitle: String){
        
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: buttonTitle, style: .default, handler: nil))
        hostViewController.present(controller, animated: true, completion: nil)
    }
}
