//
//  Constants.swift
//  Wines
//
//  Created by Arthit Thongpan on 2/8/2558 BE.
//  Copyright (c) 2558 Arthit Thongpan. All rights reserved.
//

import Foundation

let host = "192.168.1.21"
struct ServiceURL {
    static let rootURL          = "http://\(host)/workshop_slim_uploadfile/public"
    static let assets           = rootURL + "assets"
    static let upload           = "http://\(host)/workshop_slim_uploadfile/upload.php"
}
