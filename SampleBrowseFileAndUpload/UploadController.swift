//
//  UploadController.swift
//  SampleBrowseVideoInPhotoLibrary
//
//  Created by Arthit Thongpan on 8/18/16.
//  Copyright © 2016 Arthit Thongpan. All rights reserved.
//

import UIKit
import Photos

class UploadController: NSObject {
    static let shared = UploadController()
    // Private initialization to ensure just one instance is created.
    private override init() {}
    
    let url = URL(string: "http://192.168.1.21/workshop_slim_uploadfile/public/assets")!
    
    func upload(data:Data, filename:String, mediaType:PHAssetMediaType, completion:@escaping (Data?, URLResponse?, Error?)->()) {
        
        var mimeType = ""
        switch mediaType {
        case .image:
            mimeType = "image/jpeg"
        case .video:
            mimeType = "audio/mpeg"
            
        default:
            mimeType = "image/jpeg"
            
        }
        
        let mutableURLRequest = NSMutableURLRequest(url: url)
        mutableURLRequest.httpMethod = "POST"
        let phpVariable = "uploadFile"  // ตัวแปรสำหรับให้ php อ้างอิงถึง content ข้างใน
        let boundary = generateBoundaryString()
        let contentType = "multipart/form-data; boundary=" + boundary
        let boundaryStart = "--\(boundary)\r\n"
        let boundaryEnd = "--\(boundary)--\r\n"
        let contentDispositionString = "Content-Disposition: form-data; name=\"\(phpVariable)\"; filename=\"\(filename)\"\r\n"
        let contentTypeString = "Content-Type: \(mimeType)\r\n\r\n"
        
        // Prepare the HTTPBody for the request.
        let requestBodyData : NSMutableData = NSMutableData()
        requestBodyData.append(boundaryStart.data(using: String.Encoding.utf8)!)
        requestBodyData.append(contentDispositionString.data(using: String.Encoding.utf8)!)
        requestBodyData.append(contentTypeString.data(using: String.Encoding.utf8)!)
        requestBodyData.append(data as Data)
        requestBodyData.append("\r\n".data(using: String.Encoding.utf8)!)
        requestBodyData.append(boundaryEnd.data(using: String.Encoding.utf8)!)
        
        mutableURLRequest.setValue(contentType, forHTTPHeaderField: "Content-Type")
        mutableURLRequest.httpBody = requestBodyData as Data
        let session = URLSession.shared
        
        let task = session.dataTask(with: mutableURLRequest as URLRequest) {
            (
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                print("error")
                completion(nil, response, error)
                return
            }
            
            let dataString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
            
            print(dataString!)
            
            completion(data, response, error)
            
        }
        
        task.resume()
    }

    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}
